#!/bin/bash
echo "---------------------------------------------------"
echo "  SCRIPT SH PARA INSTALACION Y CONFIG DE JAVA JDK"
echo "---------------------------------------------------"
if [ $1 ];
then
RUTA_ZIP="$@"
else
echo "Por Favor Indica la ruta absoluta al tar del JDK"
read RUTA_ZIP
echo "---------------------------------------------------"
fi
#RUTAS QUE SE USARAN PARA LA INSTALACION DE JDK DE JAVA
RUTA_JAVA=/usr/lib/jvm
TMP=/tmp/java-oracle
RUTA_PATH=/etc/profile.d/jvm.sh
echo "Usaremos los siguientes datos para la Instalacion"
echo "Directorio de Instalación: $RUTA_JAVA"
echo "Directorio de Variables y PATH: $RUTA_PATH"
echo "Ruta del tar JAVA JDK: $RUTA_ZIP"
echo "---------------------------------------------------"
echo "Desea continuar con la Intalación: s/n: " 
read CONTINUAR
if [ "$CONTINUAR" != s ] && [ "$CONTINUAR" != S ]; then
echo "---------------------------------------------------"
echo "Has Cancelado la instalacion del JAVA JDK"
echo "---------------------------------------------------"
exit
fi
echo "---------------------------------------------------"
echo "Verificando las rutas de Instalación"
if [ ! -f $RUTA_ZIP ]; then
echo "El archvios tar.gz no se encuentar en la ruta que"
echo "nos has indicado, por favor verifica la rura y"
echo "instenta de nuevo. hasta luego.."
echo "---------------------------------------------------"
exit
fi
if [ ! -d $RUTA_JAVA ];then
echo "El Directorio de instalación no existe..."
echo "Creando el Directorio de Intalacion: $RUTA_JAVA"
mkdir -p $RUTA_JAVA
echo "El Directorio de Instalación fue creado..."
else
echo "El directorio de instalación existe.."
echo "Eliminando la instalación antigua"
echo "Eliminando la configuraciones de Alternativas anteriores"
#ELIMINANDO LAS CONFIGURACIONES DE ALTERNATIVAS PARA EL JDK
update-alternatives --remove java /usr/bin/java
update-alternatives --remove rmid /usr/bin/rmid
update-alternatives --remove keytool /usr/bin/keytool
update-alternatives --remove rmiregistry /usr/bin/rmiregistry
update-alternatives --remove pack200 /usr/bin/pack200
update-alternatives --remove servertool /usr/bin/servertool
update-alternatives --remove policytool /usr/bin/policytool
update-alternatives --remove orbd /usr/bin/orbd
update-alternatives --remove tnameserv /usr/bin/tnameserv
update-alternatives --remove ControlPanel /usr/bin/ControlPanel
update-alternatives --remove jcontrol /usr/bin/jcontrol
update-alternatives --remove java_vm /usr/bin/java_vm
update-alternatives --remove javaws /usr/bin/javaws
update-alternatives --remove unpack200 /usr/bin/unpack200
#ELIMINANDO LAS CONFIGURACIONES ALTERNATIVAS PARA JAVA
update-alternatives --remove jar /usr/bin/jar
update-alternatives --remove jcmd /usr/bin/jcmd
update-alternatives --remove jstat /usr/bin/jstat
update-alternatives --remove apt /usr/bin/apt
update-alternatives --remove javac /usr/bin/javac
update-alternatives --remove javah /usr/bin/javah
update-alternatives --remove javap /usr/bin/javap
update-alternatives --remove jarsigner /usr/bin/jarsigner
update-alternatives --remove javadoc /usr/bin/javadoc
echo "Eliminando Archivos antiguos"
rm -rf $RUTA_JAVA
echo "La Instalación antigua fue Eliminada"
echo "---------------------------------------------------"
fi
echo "---------------------------------------------------"
echo "     CREANDO UNA NUEVA INSTALACION DE JAVA JDK"
echo "Creando un directorio temporal para los nuevos Archivos"
mkdir -p $TMP
echo "Descomprimiendo los nuevos Archivos al directorio temporal"
echo "Ruta Temporal: $TMP"
tar -xzf  $RUTA_ZIP -C $TMP
#buscamos el nombre de la carpeta donde estan los archivos que fueron
#descomprimidos
TMP_JDK=$TMP"/"$(ls -D $TMP)
echo "Copiando los nuevos Archivos a la ruta de instalación"
mv $TMP_JDK $RUTA_JAVA
echo "Los Archivos fueron copiados a: $RUTA_JAVA"
echo "Eliminando el directorio temporal"
rm -fr $TMP
echo "Actualizando las Alternativas"
#CONFIGURANDO LAS NUEVAS ALTERNATIVAS PARA EL JDK
update-alternatives --install /usr/bin/java java $RUTA_JAVA/jre/bin/java 10
update-alternatives --install /usr/bin/rmid rmid $RUTA_JAVA/jre/bin/rmid 10
update-alternatives --install /usr/bin/keytool keytool $RUTA_JAVA/jre/bin/keytool 10
update-alternatives --install /usr/bin/rmiregistry rmiregistry $RUTA_JAVA/jre/bin/rmiregistry 10
update-alternatives --install /usr/bin/pack200 pack200 $RUTA_JAVA/jre/bin/pack200 10
update-alternatives --install /usr/bin/servertool servertool $RUTA_JAVA/jre/bin/servertool 10
update-alternatives --install /usr/bin/policytool policytool $RUTA_JAVA/jre/bin/policytool 10
update-alternatives --install /usr/bin/orbd orbd $RUTA_JAVA/jre/bin/orbd 10
update-alternatives --install /usr/bin/tnameserv tnameserv $RUTA_JAVA/jre/bin/tnameserv 10
update-alternatives --install /usr/bin/ControlPanel ControlPanel $RUTA_JAVA/jre/bin/ControlPanel 10
update-alternatives --install /usr/bin/jcontrol jcontrol $RUTA_JAVA/jre/bin/jcontrol 10
update-alternatives --install /usr/bin/java_vm java_vm $RUTA_JAVA/jre/bin/java_vm 10
update-alternatives --install /usr/bin/javaws javaws $RUTA_JAVA/jre/bin/javaws 10
update-alternatives --install /usr/bin/unpack200 unpack200 $RUTA_JAVA/jre/bin/unpack200 10
#CONFIGURANDO LAS NUEVAS ALTERNATIVAS PARA JAVA
update-alternatives --install /usr/bin/jar jar $RUTA_JAVA/bin/jar 10
update-alternatives --install /usr/bin/jcmd jcmd $RUTA_JAVA/bin/jcmd 10
update-alternatives --install /usr/bin/jstat jstat $RUTA_JAVA/bin/jstat 10
update-alternatives --install /usr/bin/apt apt $RUTA_JAVA/bin/apt 10
update-alternatives --install /usr/bin/javac javac $RUTA_JAVA/bin/javac 10
update-alternatives --install /usr/bin/javah javah $RUTA_JAVA/bin/javah 10
update-alternatives --install /usr/bin/javap javap $RUTA_JAVA/bin/javap 10
update-alternatives --install /usr/bin/jarsigner jarsigner $RUTA_JAVA/bin/jarsigner 10
update-alternatives --install /usr/bin/javadoc javadoc $RUTA_JAVA/bin/javadoc 10
echo "La configuracion de las Aternativas ha terminado"
echo "Configurando las Variables del Sistema y PATH"
if [ -f $RUTA_PATH ];then
echo "El archivo de configuraciones existe lo eliminaremos"
echo "para configurar con las nuevas variables"
rm $RUTA_PATH
echo "El archivo: $RUTA_PATH fue eliminado"
fi
echo "Creando el nuevo Archivo para las Variables y PATH"
echo "
export JAVA_HOME=$RUTA_JAVA
export PATH=\$PATH:$RUTA_JAVA/bin
export J2SDKDIR=$RUTA_JAVA
export J2REDIR=$RUTA_JAVA/jre
export PATH=\$PATH:$RUTA_JAVA/jre/bin
export DERBY_HOME=$RUTA_JAVA/db
export PATH=\$PATH:$RUTA_JAVA/db/bin
export INSTALADOR_JAVA=www.autanasoft.com">>$RUTA_PATH
echo "El Archivo a sido creado."
echo "---------------------------------------------------"
echo "LA INSTALACION DE JAVA JDK HA FINALIZADO..."
echo "---------------------------------------------------"
